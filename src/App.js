import React from 'react';
import { StyleSheet, Modal, Button, View} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import FingerprintPopup from './FingerprintPopup.component';
import stylesFingerprint from './resources/Application.container.styles';
import Microphone from './Microphone';
import GeolocationScreen from './GeolocationScreen'
import CameraScreen from './CameraScreen'
import SensorScreen from './SensorScreen'

class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: undefined,
            popupShowed: false
        };
    }

    static navigationOptions = {
        title: 'Welcome DrFirst React Native Demo!',
    };

    handleFingerprintShowed = () => {
        this.setState({ popupShowed: true });
    };

    handleFingerprintDismissed = () => {
        this.setState({ popupShowed: false });
    };

    componentDidMount() {
        FingerprintScanner
            .isSensorAvailable()
            .catch(error => this.setState({ errorMessage: error.message }));
    }

  render() {
        const { errorMessage, popupShowed } = this.state;

        return (
          <View style={styles.container}>
              <View style={styles.button}>
                  <Button
                      title="Camera"
                      onPress={() => this.props.navigation.navigate('Camera')}/>
              </View>
              <View style={styles.button}>
                  <Button
                      title="Fingerprint"
                      onPress={() => this.handleFingerprintShowed()}/>

                  {popupShowed && (
                      <Modal
                          animationType="slide"
                          transparent={true}
                          visible={true}
                          onRequestClose={() => {
                              alert('Modal has been closed.');
                          }}>
                          <FingerprintPopup
                              style={stylesFingerprint.popup}
                              handlePopupDismissed={this.handleFingerprintDismissed}
                          />
                      </Modal>
                  )}

              </View>
              <View style={styles.button}>
                  <Button
                      title="Microphone"
                      onPress={() => this.props.navigation.navigate('Microphone')}/>
              </View>
              <View style={styles.button}>
                  <Button
                      title="Geolocation"
                      onPress={() => this.props.navigation.navigate('Geolocation')}/>
              </View>
              <View style={styles.button}>
                  <Button
                      title="Sensors"
                      onPress={() => this.props.navigation.navigate('Sensor')}/>
              </View>
          </View>
        );
  }
}

const RootStack = createStackNavigator(
    {
        Home: HomeScreen,
        Camera: CameraScreen,
        Microphone: Microphone,
        Geolocation: GeolocationScreen,
        Sensor: SensorScreen
    },
    {
        initialRouteName: 'Home',
    }
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
    button: {
      margin: 10
    }
});

export default class App extends React.Component {
    render() {
        return <RootStack />;
    }
}