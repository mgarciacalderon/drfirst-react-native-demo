import React from 'react';
import {
    Alert,
    Image,
    Text,
    TouchableOpacity,
    View,
    ViewPropTypes
} from 'react-native';
import PropTypes from 'prop-types';

import FingerprintScanner from 'react-native-fingerprint-scanner';
import ShakingText from './resources/ShakingText.component';
import styles from './resources/FingerprintPopup.component.styles';

class FingerprintPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: undefined,
            popupShowed: false
        };
    }

    componentDidMount() {
        FingerprintScanner
            .authenticate({ onAttempt: this.handleAuthenticationAttempted })
            .then(() => {
                this.props.handlePopupDismissed();
                Alert.alert('Fingerprint Authentication', 'Authenticated successfully');
            })
            .catch((error) => {
                this.setState({ errorMessage: error.message });
                this.description.shake();
            });
    }

    componentWillUnmount() {
        FingerprintScanner.release();
    }

    handleAuthenticationAttempted = (error) => {
        this.setState({ errorMessage: error.message });
        this.description.shake();
    };

    render() {
        const { errorMessage } = this.state;
        const { style, handlePopupDismissed } = this.props;

        return (
            <View style={styles.container}>
                <View style={[styles.contentContainer, style]}>

                    <Image
                        style={styles.logo}
                        source={require('./resources/assets/finger_print.png')}
                    />

                    <Text style={styles.heading}>
                        Fingerprint{'\n'}Authentication
                    </Text>
                    <ShakingText
                        ref={(instance) => { this.description = instance; }}
                        style={styles.description(!!errorMessage)}>
                        {errorMessage || 'Scan your fingerprint on the\ndevice scanner to continue'}
                    </ShakingText>

                    <TouchableOpacity
                        style={styles.buttonContainer}
                        onPress={handlePopupDismissed}>

                        <Text style={styles.buttonText}>
                            BACK TO MAIN
                        </Text>

                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

FingerprintPopup.propTypes = {
    style: ViewPropTypes.style,
    handlePopupDismissed: PropTypes.func.isRequired,
};

export default FingerprintPopup;